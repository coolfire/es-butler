#!/usr/bin/env ruby
# frozen_string_literal: true

require_relative 'lib/config'
require_relative 'lib/elastic'
require 'logger'
require 'yaml'

config = load_config
logger = Logger.new(config.logfile)
es     = Elastic.new config
rules  = YAML.safe_load(File.read(config.rules))

rules.each do |n, params|
  logger.info "Submitting action: #{n}"

  case params.keys
  when ->(h) { h.include? 'delete' }
    res = es.delete(params['delete'], params['days'])
  when ->(h) { h.include? 'delete_except' }
    res = es.delete_except(params['delete_except'], params['days'])
  end
  logger.info "Received task id:  #{res['task']}"
end
