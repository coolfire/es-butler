# frozen_string_literal: true

# Class to make http requests to elasticsearch
class Elastic
  require_relative 'jsonapi'
  require 'base64'

  def initialize(config)
    @config = config
    @http   = JsonApi.new(
      "#{config.es.host}/#{config.es.index}",
      verify: false
    )
    add_auth_header @http.headers
  end

  def delete(query, days)
    @http.post(
      '_delete_by_query?wait_for_completion=false',
      make_delete_query(query, days)
    )
  end

  def delete_except(query, days)
    @http.post(
      '_delete_by_query?wait_for_completion=false',
      make_delete_except_query(query, days)
    )
  end

  def search(query, days)
    @http.post(
      '_search',
      make_search_query(query, days)
    )
  end

  private

  def add_auth_header(headers)
    auth = Base64.strict_encode64("#{@config.es.user}:#{@config.es.pass}")
    headers['Authorization'] = "Basic #{auth}"
  end

  # rubocop:disable Metrics/MethodLength
  def make_delete_query(terms, days)
    template = {
      'query' => {
        'bool' => {
          'must' => [
            {
              'range' => {
                '@timestamp' => {
                  'lt' => "now-#{days}d/d"
                }
              }
            }
          ]
        }
      }
    }

    terms.map { |t| { 'term' => t } }.each do |term|
      template['query']['bool']['must'].append term
    end

    template
  end
  # rubocop:enable Metrics/MethodLength

  # rubocop:disable Metrics/MethodLength
  # rubocop:disable Layout/HashAlignment
  def make_delete_except_query(terms, days)
    template = {
      'query' => {
        'bool' => {
          'must' => [
            {
              'range' => {
                '@timestamp' => {
                  'lt' => "now-#{days}d/d"
                }
              }
            }
          ],
          'must_not' => [{
            'bool' => {
              'must' => []
            }
          }]
        }
      }
    }

    terms.map { |t| { 'term' => t } }.each do |term|
      template['query']['bool']['must_not'][0]['bool']['must'].append term
    end

    template
  end
  # rubocop:enable Layout/HashAlignment
  # rubocop:enable Metrics/MethodLength

  def make_search_query(terms, days); end
end
