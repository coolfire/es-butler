# frozen_string_literal: true

require 'json'
require 'ostruct'
require 'yaml'

def load_config_environment(config, prefix = $PROGRAM_NAME.delete_suffix('.rb'))
  config.each_pair do |name, val|
    load_config_environment(val, "#{prefix}_#{name}") if val.is_a? Hash

    key = "#{prefix}_#{name}".upcase
    config[name] = ENV[key] if ENV.include? key
  end

  config
end

def load_config(configfile = 'config/config.yaml')
  raise("Configuration file not found: #{configfile}") unless File.exist? configfile

  yconf = YAML.safe_load(File.read(configfile))
  load_config_environment(yconf)

  JSON.parse(yconf.to_json, object_class: OpenStruct)
end
